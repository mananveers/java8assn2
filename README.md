# Java8assn2

The contained SQL file is a demonstration of SQL commands in Oracle Database 21c using SQL Plus. It includes creating a new user, creating three tables (Employee, Department, and Location), adding entries to the tables, joining the tables, and displaying selected information.