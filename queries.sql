-- Log in as the admin
connect sys as sysdba;

-- Create a new user named "TestUser" with a random 8 character password
CREATE USER TestUser IDENTIFIED BY "TestPass";

-- Grants all privileges to the new user
GRANT ALL PRIVILEGES TO TestUser;

-- Log out as the admin
DISCONNECT;

-- Log in as the new user
connect TestUser/TestPass;

-- Create Employee table
create table Employee (
  id number,
  name varchar2(50),
  age number,
  salary number,
  department_id number,
  constraint employee_department_fk foreign key (department_id) references Department(id)
);


-- Create Department table
create table Department (
  id number,
  name varchar2(50),
  location_id number,
  constraint department_location_fk foreign key (location_id) references Location(id)
);

-- Create Location table
create table Location (
  id number primary key,
  name varchar2(50)
);

-- Command to add foreign key if it had not been added
--alter table Department
--add constraint fk_department_location
--foreign key (location_id) references Location (id);

-- Generate Sequence
create sequence location_seq start with 1 increment by 1;

-- Fill tables with five values each
insert into Location values (location_seq.nextval, 'Albuquerque');
insert into Location values (location_seq.nextval, 'Los Pollos Hermanos');
insert into Location values (location_seq.nextval, 'Superlab');
insert into Location values (location_seq.nextval, 'Desert');
insert into Location values (location_seq.nextval, 'Car Wash');

insert into Department values (1, 'Production', 3);
insert into Department values (2, 'Restaurant Operations', 2);
insert into Department values (3, 'Waste Management', 1);
insert into Department values (4, 'Lab Operations', 4);
insert into Department values (5, 'Finance', 5);

insert into Employee values (1, 'Walter White', 50, 100000, 1);
insert into Employee values (2, 'Gus Fring', 55, 200000, 2);
insert into Employee values (3, 'Jesse Pinkman', 25, 50000, 1);
insert into Employee values (4, 'Hank Schrader', 45, 60000, 3);
insert into Employee values (5, 'Skyler White', 40, 65000, 5);

-- Display employee with highest salary
select name, salary
from Employee
where salary = (select max(salary) from Employee);

-- Display employee with second highest salary
select name, salary
from (
  select name, salary, dense_rank() over (order by salary desc) as salary_rank
  from Employee
)
where salary_rank = 2;

-- Join the three tables with some common values
select Employee.name as EmployeeName, Location.name as LocationName, Department.name as DepartmentName
from Employee
join Department on Employee.department = Department.id
join Location on Department.location = Location.id;
